# Cron Challenge

## What is this?

```
$ cargo run --quiet '5-10 0,12 */2 * * /bin/somebinary'
minute        5 6 7 8 9 10
hour          0 12
day of month  1 3 5 7 9 11 13 15 17 19 21 23 25 27 29 31
month         1 2 3 4 5 6 7 8 9 10 11 12
day of week   0 1 2 3 4 5 6
command       /bin/somebinary
```

This tool allows you to see the generated times which your cron job will run at.

## How do I get started with development?

You'll need to install the rust toolchain manager [rustup](https://rustup.rs/).

You'll then need to install rust:

```
rustup toolchain install stable
```

Then it's as simple as:

```
cargo run '1-5 3,6,9 * * * /bin/somebinary'
```

This will compile and run the binary.

You can run the tests and generate a coverage report using:

```
cargo install cargo-tarpaulin && cargo tarpaulin --exclude-files=main.rs -o Html
```

An HTML report will be generated: `./tarpaulin-report.html`.

## How do I run this?

You can use the `cargo run` option as shown above, or if you'd like you can use the docker image.

```
docker build -t cron_challenge . && docker run cron_challenge '1-5 3,6,9 * * * /bin/somebinary'
```
