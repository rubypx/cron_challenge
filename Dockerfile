# Build container
FROM rust:1.45.2 as builder

WORKDIR /usr/src/app
COPY . .

RUN rustup target add x86_64-unknown-linux-musl

# # Runs tests and generates coverage - unfortunately this fails miserably within docker
# # https://github.com/xd009642/tarpaulin/issues/406
# RUN cargo install cargo-tarpaulin && cargo tarpaulin --exclude-files=main.rs
RUN cargo test -- --color=always
RUN cargo install --path . --target x86_64-unknown-linux-musl

# Runtime container
FROM alpine
COPY --from=builder /usr/local/cargo/bin/cron_challenge /bin/cron_challenge
ENTRYPOINT ["/bin/cron_challenge"]
