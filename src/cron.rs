use std::ops::RangeInclusive;

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn parse_component_from_input_string_valid_single() {
        assert_eq!(vec![40], parse_component("40", 0..=59));
    }

    #[test]
    #[should_panic(expected = "Unable to parse cron line: Cron line was invalid")]
    fn parse_args_from_invalid_string() {
        parse_args("invalid");
    }

    #[test]
    #[should_panic(expected = "Entry out of range 0..=59\nOffender: 60")]
    fn parse_component_from_input_string_out_of_range_single() {
        parse_component("60", 0..=59);
    }

    #[test]
    #[should_panic(
        expected = "Unable to parse multi: invalid digit found in string\nOffender: invalid"
    )]
    fn parse_component_from_input_string_invalid_single() {
        parse_component("invalid", 0..=59);
    }

    #[test]
    fn parse_component_from_input_string_valid_wildcard_single() {
        assert_eq!((0..=59).collect::<Vec<u8>>(), parse_component("*", 0..=59));
    }

    #[test]
    fn parse_component_from_input_string_valid_fractional() {
        assert_eq!(
            vec![0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55],
            parse_component("*/5", 0..=59)
        );
    }

    #[test]
    fn parse_component_from_input_string_valid_range() {
        assert_eq!((1..=10).collect::<Vec<u8>>(),
            parse_component("1-10", 0..=59));
    }

    #[test]
    #[should_panic(
        expected = "Unable to parse range extremity invalid digit found in string\nOffender: invalid"
    )]
    fn parse_component_from_input_string_invalid_range() {
        parse_component("1-invalid", 0..=59);
    }

    #[test]
    #[should_panic(
        expected = "Range 1..=10: is not contained within it\'s parent range 0..=9"
    )]
    fn parse_component_from_input_string_invalid_range_subset() {
        parse_component("1-10", 0..=9);
    }

    #[test]
    #[should_panic(
        expected = "Unable to split range: [\"invalid\"]"
    )]
    fn parse_range_from_input_string_invalid_range() {
        parse_range("invalid", 0..=9);
    }

    #[test]
    #[should_panic(
        expected = "Unable to parse multi: invalid digit found in string\nOffender: invalid"
    )]
    fn parse_component_from_input_string_invalid_fractional() {
        parse_component("invalid", 0..=59);
    }

    #[test]
    #[should_panic(expected = "Numerator out of range 0..=59\nOffender: 60")]
    fn parse_component_from_input_string_out_of_range_fractional() {
        parse_component("60/5", 0..=59);
    }

    #[test]
    fn parse_component_from_input_string_valid_multi() {
        assert_eq!(vec![0, 40], parse_component("0,40", 0..=59));
    }

    #[test]
    fn parse_args_from_string() {
        // Formatting weirdness here is due to coverage bug when split over multiple lines
        assert_eq!(CronEntry {
            minutes: Minutes {
                entries: vec![0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55]
            },
            hours: Hours {
                entries: vec![0, 12]
            },
            days: Days { entries: vec![1] },
            months: Months {
                entries: vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
            },
            days_of_week: DaysOfWeek {
                entries: vec![0, 1, 2, 3, 4, 5, 6]
            },
            command: &"test".to_string(),
        },
        parse_args("*/5 0,12 1 * * test"));
    }

    #[test]
    #[should_panic(expected = "Unable to parse multi: invalid digit found in string\nOffender: *")]
    fn parse_component_from_input_string_invalid_multi() {
        parse_component("0,*", 0..=59);
    }

    #[test]
    #[should_panic(expected = "Unable to split fraction: [\"invalid\"]")]
    fn parse_fractional_from_invalid_input_string() {
        parse_fractional("invalid", 0..=59);
    }

    #[test]
    #[should_panic(
        expected = "Unable to parse numerator invalid digit found in string\nOffender: invalid"
    )]
    fn parse_numerator_from_invalid_input_string() {
        parse_numerator("invalid", 0..=59);
    }

    #[test]
    fn parse_numerator_from_valid_input_string() {
        assert_eq!(59, parse_numerator("59", 0..=59));
    }
}

#[derive(Debug, PartialEq)]
pub struct CronEntry<'a> {
    pub minutes: Minutes,
    pub hours: Hours,
    pub days: Days,
    pub months: Months,
    pub days_of_week: DaysOfWeek,
    pub command: &'a str,
}

#[derive(Debug, PartialEq)]
pub struct Minutes {
    pub entries: Vec<u8>,
}

#[derive(Debug, PartialEq)]
pub struct Hours {
    pub entries: Vec<u8>,
}

#[derive(Debug, PartialEq)]
pub struct Days {
    pub entries: Vec<u8>,
}

#[derive(Debug, PartialEq)]
pub struct Months {
    pub entries: Vec<u8>,
}

#[derive(Debug, PartialEq)]
pub struct DaysOfWeek {
    pub entries: Vec<u8>,
}

trait CronComponent {
    fn parse(input: &str) -> Self;
}

impl CronComponent for Minutes {
    fn parse(input: &str) -> Minutes {
        Minutes {
            entries: parse_component(input, 0..=59),
        }
    }
}
impl CronComponent for Hours {
    fn parse(input: &str) -> Hours {
        Hours {
            entries: parse_component(input, 0..=23),
        }
    }
}

impl CronComponent for Days {
    fn parse(input: &str) -> Days {
        Days {
            entries: parse_component(input, 1..=31),
        }
    }
}

impl CronComponent for Months {
    fn parse(input: &str) -> Months {
        Months {
            entries: parse_component(input, 1..=12),
        }
    }
}

impl CronComponent for DaysOfWeek {
    fn parse(input: &str) -> DaysOfWeek {
        DaysOfWeek {
            entries: parse_component(input, 0..=6),
        }
    }
}

fn parse_component(input: &str, range: RangeInclusive<u8>) -> Vec<u8> {
    if input.contains("/") {
        parse_fractional(input, range)
    } else if input.contains("-") {
        parse_range(input, range)
    } else {
        parse_multi(input, range)
    }
}

fn parse_numerator(input: &str, range: RangeInclusive<u8>) -> u8 {
    match input {
        "*" => *range.start(),
        _ => {
            let numerator: u8 = input.parse().unwrap_or_else(|err| {
                panic!("Unable to parse numerator {}\nOffender: {}", err, input)
            });
            if !range.contains(&numerator) {
                panic!(
                    "Numerator out of range {:?}\nOffender: {}",
                    range, numerator
                )
            }

            numerator
        }
    }
}

fn parse_denominator(input: &str) -> u8 {
    input
        .parse()
        .unwrap_or_else(|err| panic!("Unable to parse denominator {}\nOffender: {}", err, input))
}

fn parse_range_extremity(input: &str) -> u8 {
    input
        .parse()
        .unwrap_or_else(|err| panic!("Unable to parse range extremity {}\nOffender: {}", err, input))
}

fn parse_fractional(input: &str, range: RangeInclusive<u8>) -> Vec<u8> {
    let raw_fraction = input.split("/").take(2).collect::<Vec<&str>>();
    if let [raw_numerator, raw_denominator] = raw_fraction.as_slice() {
        generate_fractional(
            parse_numerator(raw_numerator, range.clone()),
            parse_denominator(raw_denominator),
            range,
        )
    } else {
        panic!("Unable to split fraction: {:?}", raw_fraction);
    }
}

fn generate_fractional(starting_at: u8, interval: u8, range: RangeInclusive<u8>) -> Vec<u8> {
    let mut entries: Vec<u8> = vec![];
    let mut current_entry = starting_at;
    while range.contains(&current_entry) {
        entries.push(current_entry);
        current_entry += interval;
    }

    entries
}

fn parse_multi(input: &str, range: RangeInclusive<u8>) -> Vec<u8> {
    match input {
        "*" => range.collect(),
        _ => input
            .split(',')
            .map(|chunk| {
                let entry: u8 = chunk.parse().unwrap_or_else(|err| {
                    panic!("Unable to parse multi: {}\nOffender: {}", err, chunk)
                });
                if !range.contains(&entry) {
                    // Rust ranges are exclusive so `0..60` does not include 60 (but does include 0)
                    panic!("Entry out of range {:?}\nOffender: {}", range, entry)
                }

                entry
            })
            .collect(),
    }
}

fn parse_range(input: &str, range: RangeInclusive<u8>) -> Vec<u8> {
    let raw_range = input.split("-").take(2).collect::<Vec<&str>>();
    if let [raw_start, raw_end] = raw_range.as_slice() {
        let generated = parse_range_extremity(raw_start)..=parse_range_extremity(raw_end);
        if range.contains(&generated.start()) && range.contains(&generated.end()) {
            generated.collect()
        } else {
            panic!("Range {:?}: is not contained within it's parent range {:?}", generated, range);
        }
    } else {
        panic!("Unable to split range: {:?}", raw_range);
    }
}

pub fn parse_args(cron_line: &str) -> CronEntry {
    let cron_components: Vec<&str> = cron_line.split_whitespace().take(6).collect();

    match cron_components.as_slice() {
        // TODO: Allow commands with their own arguments
        [minutes_raw, hours_raw, days_raw, months_raw, days_of_week_raw, command] => {
            Ok(CronEntry {
                minutes: Minutes::parse(minutes_raw),
                hours: Hours::parse(hours_raw),
                days: Days::parse(days_raw),
                months: Months::parse(months_raw),
                days_of_week: DaysOfWeek::parse(days_of_week_raw),
                command: &command,
            })
        },
        [_, _, _, _, _] => Err("Missing command"),
        _ => Err("Cron line was invalid"),
    }
    .unwrap_or_else(|err| panic!("Unable to parse cron line: {}", err))
}
