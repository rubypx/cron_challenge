use std::env;
mod cron;

fn main() {
    let cron_args = env::args().nth(1);
    let cron_string = cron_args.unwrap_or_else(|| panic!("Please provide a cron string"));
    let cron = cron::parse_args(&cron_string);

    print!(
        // Cannot be dynamic because of print! macro restrictions, could be reimplemented with a
        // custom padding function.
        "{: <14}{}\n\
        {: <14}{}\n\
        {: <14}{}\n\
        {: <14}{}\n\
        {: <14}{}\n\
        {: <14}{}\n",
        "minute",
        format_u8_list(cron.minutes.entries),
        "hour",
        format_u8_list(cron.hours.entries),
        "day of month",
        format_u8_list(cron.days.entries),
        "month",
        format_u8_list(cron.months.entries),
        "day of week",
        format_u8_list(cron.days_of_week.entries),
        "command",
        cron.command,
    );
}

fn format_u8_list(u8_list: Vec<u8>) -> String {
    u8_list.iter().map(|m| m.to_string() ).collect::<Vec<String>>().join(" ")
}
